UglifyJS Bug:

I seriously need `mqtt.js` and when
 I import the `connect` function of `mqtt.js` and type `npm run build` for build production stuffs
 something fell in error and the `uglify.js` can not finish its job.
 
For running my sample project just run:

1. `npm install`
2. `npm run dev`

For building production stuffs just run:

1. `npm run build`

This code fall in error because of using `mqtt.js` and `uglify.js` can not finish its job
and the `server.js` file in `dist` folder is ok and could be run in server but it is not
uglified and compressed also the build process fall in a error and it is not good for many reasons.

If you know why it is happen please tell me in [**`this link`**](https://stackoverflow.com/questions/50905995/webpack-uglifyjs-fall-in-error-for-production-build-because-of-mqtt-js-existance) 
